﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public class ProjectileManager
    {
        private Dictionary<Guid, Projectile> projectiles = new Dictionary<Guid, Projectile>();
        private int CreatedProjectileCount = 0;
        public ProjectileManager() { }
        
        public void AddProjectile(Projectile newProjectile)
        {
            CreatedProjectileCount++;
            projectiles[newProjectile.Id] = newProjectile;
        }
        public Projectile GetProjectile(Guid id)
        {
            return projectiles[id];
        }
        public void UpdateProjectiles(RenderInfo render, ConcurrentDictionary<Point, ConcurrentStack<Synapse>> synapses)
        {
            List<Guid> projectilesToDie = new List<Guid>();
            foreach(var key in projectiles.Keys)
            {
               var shouldDie =  projectiles[key].Update(render, synapses);
               if (shouldDie)
               {
                    projectilesToDie.Add(key);
               }
            }
            foreach(var key in projectilesToDie)
            {
                projectiles.Remove(key);
            }
        }
        public void DrawProjectiles(Graphics g, RenderInfo render)
        {
            foreach(var key in projectiles.Keys)
            {
                projectiles[key].Draw(g, render);
            }
        }

    }
}
