﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonima
{
    public class NeuronFactoryUIController
    {
        private int _neuronsToBeGenerated;
        private int _neuronsSoFar;
        private Stopwatch _timer;
        private BackgroundWorker _worker;
        public NeuronFactoryUIController(BackgroundWorker worker, int neuronsToBeGenerated)
        {

            _neuronsToBeGenerated = neuronsToBeGenerated;
            //_prgBar.Maximum = _neuronsToBeGenerated;
            _timer = new Stopwatch();
            _worker = worker;
        }

        public void IncrementNeuronCount()
        {
            _neuronsSoFar++;
            if(_neuronsSoFar % 1000 == 0)
            {
                var fraction = ((double)_neuronsSoFar / (double)_neuronsToBeGenerated) * 100.0f;
                _worker.ReportProgress((int)fraction, new NeuronFactoryUIProgressReport(_neuronsSoFar, _neuronsToBeGenerated));
                //don't flood the message queue with updates
            }
            
        }
    }
}
