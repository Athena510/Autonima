﻿using System;
using System.Numerics;

namespace Autonima
{
    public class ConePropogationFunction : ISignalPropogationFunction
    {

        public Vector2[] GetPropogationPoints(Vector2 origin, Vector2 target)
        {
            if (origin == target) return new Vector2[] { target, target, origin };
            var difference = origin - target;
            var distance = Vector2.Distance(origin, target) * 0.1f;
            var left = Vector2.Normalize(new Vector2(-difference.Y, difference.X));
            var right = Vector2.Normalize(new Vector2(-difference.Y, difference.X));
            Vector2[] output = { target - (left* distance), target - (right * -distance), origin};
            return output;
        }
        public bool IsPointInPropogationPoints(Vector2 input, Vector2[] points)
        {
            //ToDo: Fix variable cancer
            var p0 = points[0];
            var p1 = points[1];
            var p2 = points[2];
            var p = input;

            var p1Mod = new Vector2(p1.X - p0.X, p1.Y - p0.Y);
            var p2Mod = new Vector2(p2.X - p0.X, p2.Y - p0.Y);
            var pMod = new Vector2(p.X - p0.X, p.Y - p0.Y);


            var scalarDistance = (p1Mod.X * p2Mod.Y) - (p2Mod.X * p1Mod.Y);

            var w1 = ((pMod.X * (p1Mod.Y - p2Mod.Y)) + (pMod.Y * (p2Mod.X - p1Mod.X)) + (p1Mod.X * p2Mod.Y) - (p2Mod.X * p1Mod.Y))
                / scalarDistance;
            var w2 = ((pMod.X * p2Mod.Y) - (pMod.Y * p2Mod.X))
                / scalarDistance;
            var w3 = ((pMod.Y * p1Mod.X) - (pMod.X * p1Mod.Y))
                / scalarDistance;
            bool w1Check = w1 > 0 && w1 < 1;
            bool w2Check = w2 > 0 && w2 < 1;
            bool w3Check = w3 > 0 && w3 < 1;

            return w1Check && w2Check && w3Check;
        }
    }
}
