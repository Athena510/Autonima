﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public static class ExtensionMethods
    {
        public static double Distance(this Point point, Point other)
        {
            return Math.Sqrt(Math.Pow(point.X - other.X, 2) + Math.Pow(point.Y - other.Y, 2));
        }
        public static PointF Add(this PointF operand1, PointF operand2)
        {
            return new PointF(operand1.X + operand2.X, operand1.Y + operand2.Y);
        }
        public static PointF ToPointF(this Vector2 operand)
        {
            return new PointF(operand.X, operand.Y);
        }
        public static Point ToPointTruncate(this Vector2 operand)
        {
            return new Point((int)operand.X, (int)operand.Y);
        }

    }
}
