﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Autonima
{
    public class PerceptMotionActuator : IActuator
    {
        private List<Neuron> _allNeuronsInActuator;
        private Action<Vector2> _actuatorAction;
        private List<List<Neuron>> _partitions;
        /// <summary>
        /// Creates a new motion actuator that shifts in XY coordinate space based on neuron behavior.
        /// </summary>
        /// <param name="allNeuronsInActuator">Expecting 32 groups of neurons</param>
        /// <param name="actuatorAction">What gets fired. Typically a linkup to a sensor</param>
        public PerceptMotionActuator(List<Neuron> allNeuronsInActuator, Action<Vector2> actuatorAction)
        {
            _allNeuronsInActuator = allNeuronsInActuator;
            _actuatorAction = actuatorAction;
            _partitions = MoreLinq.MoreEnumerable.Batch(allNeuronsInActuator,(int) (allNeuronsInActuator.Count / 4), s => s.ToList()).ToList();


        }
        public void Actuate(Neuron actuatedNeuron)
        {
            //we split the neurons into groups, then assign a direction based on where they are in the partition
            //we do this because we need to encode moving in 4 different directions
            var unitVectors = new List<Vector2>();
            unitVectors.Add(Vector2.UnitX);
            unitVectors.Add(Vector2.UnitY);
            unitVectors.Add(-Vector2.UnitX);
            unitVectors.Add(-Vector2.UnitY);
            var total = Vector2.Zero;
            for (int i=0; i < 4; i++)
            {
                
                foreach(var n in _partitions[i])
                {
                    if(actuatedNeuron == n)
                    {
                        total = unitVectors[i];
                        _actuatorAction.Invoke(total);
                        return;
                    }
                }
            }
            
        }

        public List<Neuron> GetNeuronsToActivate()
        {
            return _allNeuronsInActuator;
        }
    }
}
