﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{

    /// <summary>
    /// This is a layer of abstraction between Synapse and Neuron.
    /// </summary>
    public class SynapseActivationAction
    {
        public float Weight { get; private set; }
        public Vector2 location { get; private set; }
        //ToDo: more info in this class?

        public SynapseActivationAction(float weight, Vector2 location)
        {
            Weight = weight;
            this.location = location;
        }

        
    }
}
