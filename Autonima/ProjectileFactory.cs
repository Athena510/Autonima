﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    /// <summary>
    /// Simple projectile factory that uses some fixed values
    /// </summary>
    public class ProjectileFactory : IProjectileFactory
    {

        public Projectile MakeProjectile(Vector2 sourceLoc, Vector2 targetLoc, float speed, bool isSourceSignal)
        {
            var vSourceTarget = targetLoc - sourceLoc;
            var normalized = Vector2.Normalize(vSourceTarget);
            
            return new Projectile(sourceLoc, speed * normalized, targetLoc, Guid.NewGuid(), 10.0f, isSourceSignal);
        }

    }
}
