﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    class AxonPulseQueue
    {
        private Queue<int> pulses { get; set; }

        public AxonPulseQueue()
        {
            pulses = new Queue<int>();
        }
        public void PushPulse(int ticksBetweenPulses)
        {
            pulses.Enqueue(ticksBetweenPulses);
        }


    }
}
