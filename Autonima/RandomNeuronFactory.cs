﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public class RandomNeuronFactory : INeuronFactory
    {
        private RenderInfo _render;
        private IProjectileFactory _projectileFactory;
        private ISynapseFactory _synapseFactory;
        private ISignalPropogationFunction _signalProp;
        private NeuronFactoryUIController _uiController;
        public RandomNeuronFactory(RenderInfo render, IProjectileFactory projectileFactory, ISynapseFactory synapseFactory, ISignalPropogationFunction signalPropogator, NeuronFactoryUIController uiController)
        {
            _render = render;
            _projectileFactory = projectileFactory;
            _synapseFactory = synapseFactory;
            _signalProp = signalPropogator;
            _uiController = uiController;
        }

        /// <summary>
        /// In order to prevent a second costly iteration over data, synapses are generated in the same pass as the neurons.
        /// Synapses 
        /// </summary>
        /// <returns>A List of Neurons</returns>
        public Tuple<ConcurrentDictionary<Point, ConcurrentStack<Neuron>>, ConcurrentDictionary<Point, ConcurrentStack<Synapse>>> GenerateNeurons(int count)
        {
            var outNeurons = new ConcurrentDictionary<Point, ConcurrentStack<Neuron>>();
            var outSynapses = new ConcurrentDictionary<Point, ConcurrentStack<Synapse>>();
            Parallel.For(0, count, a =>
            {
                var location = RandomTools.MakeRandomVector(_render.Bounds.Width, _render.Bounds.Height);
                var target = RandomTools.MakeRandomVector(_render.Bounds.Width, _render.Bounds.Height);

                var neuron = new Neuron(location, target, _projectileFactory, 5000, new SummationAxonActivationAction(50.0f)); //ToDo: Random activation weights

                var synapses = _synapseFactory.MakeSynapsePopulation(neuron, 500, location, new SizeF(500, 500), _render.Bounds);
                var grouped = synapses.GroupBy(s => s.GetGridLocation(_render.SizePerGridSquare));

                foreach(var group in grouped)
                {
                    ConcurrentStack<Synapse> synapsesInSquare = null;

                    if (outSynapses.TryGetValue(group.Key, out synapsesInSquare))
                    {
                        outSynapses[group.Key].PushRange(group.ToArray());
                    }
                    else
                    {
                        outSynapses[group.Key] = new ConcurrentStack<Synapse>();
                        outSynapses[group.Key].PushRange(group.ToArray());
                    }
                }

                
                ConcurrentStack<Neuron> neuronsInSquare = null;
                var gridSquare = neuron.GetGridLocation(_render.SizePerGridSquare);
                if (outNeurons.TryGetValue(gridSquare, out neuronsInSquare))
                {
                    neuronsInSquare.Push(neuron);
                } else
                {
                    outNeurons[gridSquare] = new ConcurrentStack<Neuron>();
                    outNeurons[gridSquare].Push(neuron);
                }
                _uiController.IncrementNeuronCount();
            });
            return new Tuple<ConcurrentDictionary<Point, ConcurrentStack<Neuron>>, ConcurrentDictionary<Point, ConcurrentStack<Synapse>>>(outNeurons, outSynapses);
        }
        
        public NeuronFactoryUIController GetUIController()
        {
            return _uiController;
        }
    }
}
