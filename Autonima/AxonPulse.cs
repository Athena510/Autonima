﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public class AxonPulse
    {
        int _pulseDurationInTicks = 0;
        float _finalTargetIntensity = 0.0f;

        public AxonPulse(int pulseDurationInTicks, float finalTargetIntensity)
        {
            _pulseDurationInTicks = pulseDurationInTicks;
            _finalTargetIntensity = finalTargetIntensity;
        }
    }
}
