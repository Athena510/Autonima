﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public interface ISignalPropogationFunction
    {
        Vector2[] GetPropogationPoints(Vector2 origin, Vector2 target);
        bool IsPointInPropogationPoints(Vector2 p, Vector2[] points);
    }
}
