﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public interface IAxonActivationCondition
    {
        void Fire(SynapseActivationAction synapsesToFire); //ToDo: replace wtih AxonPulseQueue
        bool ReadyToActivate();
        AxonPulse Activate();
        void Update();
    }
}
