﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public class RandomSynapseFactory : ISynapseFactory
    {
        public Synapse MakeSynapse(Neuron parent, Vector2 center, SizeF size, RectangleF bounds)
        {
            //get random point within Vector2
            var topLeft = new Vector2(center.X - size.Width / 2, center.Y - size.Height / 2);
            var randomPoint = RandomTools.MakeRandomVector(size.Width, size.Height);
            var clamped = Vector2.Clamp((topLeft + randomPoint), Vector2.Zero, new Vector2(bounds.Width, bounds.Height));
            return new Synapse(parent, topLeft + randomPoint);
        }

        public ConcurrentStack<Synapse> MakeSynapsePopulation(Neuron parent, int count, Vector2 center, SizeF size, RectangleF bounds)
        {
            var output = new ConcurrentStack<Synapse>();
            for(int i=0; i<count; i++)
            {
                output.Push(MakeSynapse(parent, center, size, bounds));
            }
            return output;
        }
    }
}
