﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public abstract class Entity
    {
        public Vector2 Location { get; protected set; }
        protected Vector2 initialLocation { get; private set; }
        public Entity(Vector2 location)
        {
            Location = location;
            initialLocation = Location;
        }
        public Point GetGridLocation(SizeF SizePerGridSquare)
        {
            var gridTypeFloat = new PointF(Location.X / SizePerGridSquare.Width, Location.Y / SizePerGridSquare.Height);
            return new Point((int)Math.Round(gridTypeFloat.X), (int)Math.Round(gridTypeFloat.Y));
        }
        public Point GetGridLocation(SizeF SizePerGridSquare, Vector2 targetPoint) {
            var gridTypeFloat = new PointF(targetPoint.X / SizePerGridSquare.Width, targetPoint.Y / SizePerGridSquare.Height);
            return new Point((int) Math.Round(gridTypeFloat.X), (int) Math.Round(gridTypeFloat.Y));
        }
    }
}
