﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public class NeuronFactoryUIProgressReport
    {
        public NeuronFactoryUIProgressReport(int neuronsSoFar, int totalNeurons)
        {
            NeuronsSoFar = neuronsSoFar;
            TotalNeurons = totalNeurons;
        }

        public int NeuronsSoFar { get; set; }
       public int TotalNeurons { get; set; }
    }
}
